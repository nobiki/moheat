/*
moheat.js
Version 0.8 (Update 2013/01/23)

Copyright (c) 2013 7me (http://7me.oji.0j0.jp)

Released under an MIT-style license.

*/

/* moheat.js Config start */
var conf = new Array();
conf["max_count"]			= 200;
conf["rec_query"]			= true;
/* moheat.js Config end */

var line = "[";

function mhClick(target) {
	var cnt = 0;
	var url = location.href;
	
	if(true != conf["rec_query"])
	{
		url = url.split("?");
		url = url[0];
	}
	
	$(target).bind("click", function (e){
		cnt++;
		
		// 初期化
		if(1 == cnt)
		{
			line = line + "{\"url\": \""+url+"\", \"target\": \""+target+"\", \"useragent\": \""+navigator.userAgent+"\", \"data\":[";
			//console.log("{\"url\": \""+url+"\", \"target\": \""+target+"\", \"useragent\": \""+navigator.userAgent+"\", \"data\":[");
		}
		
		// 座標計算
		var ofs = $(target).offset();
		var ofX = e.pageX - parseInt(ofs.left);
		var ofY = e.pageY - parseInt(ofs.top);

		// 追加
		if(cnt < conf["max_count"])
		{
			line = line + "{\"x\": "+ofX+",\"y\": "+ofY+"},";
			//console.log("{\"x\": "+ofX+",\"y\": "+ofY+"},");
		}
	});
}

function mhSave() {
	line = line.substr(0, line.length-1);
	
	if("" != line)
	{
		line = line +  "]}]";
		
		//console.log("Ajax Start");
		
		// 追加
		$.ajax({
			type: "POST",
			async: false,
			url: "/sample/moheat.php",
			data: "line="+line,
			success: function(msg){
			}
		});
		
		//console.log("Ajax End");
	}
	
	//return false;
	
}

$(document).ready(function(){
	mhClick("#moheatTrackingArea");
});

window.onbeforeunload = mhSave;
